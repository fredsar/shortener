package shortener

import (
	"encoding/json"
	"net/http"

	"gopkg.in/yaml.v2"
)

type (
	URLMapper struct {
		Path string `yaml:"path" json:"path"`
		URL  string `yaml:"url" json:"url"`
	}
)

// MapHandler will return an http.HandlerFunc (which also
// implements http.Handler) that will attempt to map any
// paths (keys in the map) to their corresponding URL (values
// that each key in the map points to, in string format).
// If the path is not provided in the map, then the fallback
// http.Handler will be called instead.
func MapHandler(pathsToUrls map[string]string, fallback http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		url := pathsToUrls[r.URL.Path]
		if url != "" {
			http.Redirect(w, r, url, http.StatusMovedPermanently)
		}
		fallback.ServeHTTP(w, r)
	}
}

func parseYAML(yml []byte) ([]URLMapper, error) {
	parsed := make([]URLMapper, 0)
	err := yaml.Unmarshal(yml, &parsed)
	if err != nil {
		return nil, err
	}
	return parsed, nil
}

func buildMap(mapper []URLMapper) map[string]string {
	mp := make(map[string]string, 0)
	for _, v := range mapper {
		mp[v.Path] = v.URL
	}
	return mp
}

func parseJSON(content []byte) ([]URLMapper, error) {
	parsed := make([]URLMapper, 0)
	err := json.Unmarshal(content, &parsed)
	if err != nil {
		return nil, err
	}
	return parsed, nil
}

// YAMLHandler will parse the provided YAML and then return
// an http.HandlerFunc (which also implements http.Handler)
// that will attempt to map any paths to their corresponding
// URL. If the path is not provided in the YAML, then the
// fallback http.Handler will be called instead.
//
// YAML is expected to be in the format:
//
//     - path: /some-path
//       url: https://www.some-url.com/demo
//
// The only errors that can be returned all related to having
// invalid YAML data.
//
// See MapHandler to create a similar http.HandlerFunc via
// a mapping of paths to urls.
func YAMLHandler(yaml []byte, fallback http.Handler) (http.HandlerFunc, error) {
	parsedYaml, err := parseYAML(yaml)
	if err != nil {
		return nil, err
	}
	pathMap := buildMap(parsedYaml)
	return MapHandler(pathMap, fallback), nil
}

func JSONHandler(jsonContent []byte, fallback http.Handler) (http.HandlerFunc, error) {
	parsedJSON, err := parseJSON(jsonContent)
	if err != nil {
		return nil, err
	}
	pathMath := buildMap(parsedJSON)
	return MapHandler(pathMath, fallback), nil
}
