package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"gitlab.com/fredsar/shortener"
)

func main() {
	ymlPath := flag.String("yml", "", "the path to yml file containing the map of url")
	jsonPath := flag.String("json", "", "the path to json file containing the map of url")
	flag.Parse()
	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "Hello, world!")
	})

	// Build the MapHandler using the mux as the fallback
	pathsToUrls := map[string]string{
		"/urlshort-godoc": "https://godoc.org/github.com/gophercises/urlshort",
		"/yaml-godoc":     "https://godoc.org/gopkg.in/yaml.v2",
	}
	defaultHandler := shortener.MapHandler(pathsToUrls, mux)
	var err error
	defaultHandler, err = setJSONRoutes(*jsonPath, defaultHandler)
	if err != nil {
		panic(err)
	}
	defaultHandler, err = setYMLRoutes(*ymlPath, defaultHandler)
	if err != nil {
		panic(err)
	}
	// Build the YAMLHandler using the mapHandler as the
	// fallback
	fmt.Println("Starting the server on :8080")
	http.ListenAndServe(":8080", defaultHandler)
}

func setJSONRoutes(jsonPATH string, h http.HandlerFunc) (http.HandlerFunc, error) {
	if jsonPATH == "" {
		return h, nil
	}
	jsonContent, err := readFile(jsonPATH)
	if err != nil {
		return nil, err
	}
	return shortener.JSONHandler(jsonContent, h)
}

func setYMLRoutes(ymlPATH string, h http.HandlerFunc) (http.HandlerFunc, error) {
	if ymlPATH == "" {
		return h, nil
	}
	ymlContent, err := readFile(ymlPATH)
	if err != nil {
		return nil, err
	}
	return shortener.YAMLHandler(ymlContent, h)
}

func readFile(filePath string) ([]byte, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	return ioutil.ReadAll(file)
}
